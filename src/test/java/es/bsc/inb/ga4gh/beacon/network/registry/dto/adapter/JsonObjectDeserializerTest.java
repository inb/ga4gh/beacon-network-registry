package es.bsc.inb.ga4gh.beacon.network.registry.dto.adapter;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class JsonObjectDeserializerTest {
    
    @Test
    public void test() {
        
        final String json = "{\"info\":{\"attribute\":\"value\"}}";
        
        final Jsonb jsonb = JsonbBuilder.create();
        final OrganizationTestClass tc = jsonb.fromJson(json, OrganizationTestClass.class);
        
        Assert.assertNotNull(tc);
        Assert.assertEquals("{\"attribute\":\"value\"}", tc.info);
    }
    
    public static class OrganizationTestClass {
        @JsonbTypeDeserializer(JsonObjectDeserializer.class)
        public String info;
    }
}
