/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.registry.dto;

import es.bsc.inb.ga4gh.beacon.network.registry.dto.adapter.JsonObjectDeserializer;
import es.bsc.inb.ga4gh.beacon.network.registry.dto.adapter.JsonObjectSerializer;
import java.io.Serializable;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Dmitry Repchevsky
 */

@Entity
@Table(name = "service_organization")
public class SERVICE_ORGANIZATION implements Serializable {
    
    private String id;
    private String name;
    private String description;
    private String address;
    private String welcomeUrl;
    private String contactUrl;
    private String logoUrl;
    private String info;
        
    public SERVICE_ORGANIZATION() {}

    public SERVICE_ORGANIZATION(
            final String id,
            final String name,
            final String description,
            final String address,
            final String welcomeUrl,
            final String contactUrl,
            final String logoUrl) {
        
        this.id = id;
        this.name = name;
        this.description = description;
        this.address = address;
        this.welcomeUrl = welcomeUrl;
        this.contactUrl = contactUrl;
        this.logoUrl = logoUrl;
    }

    @Id
    @Column(nullable = false)
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getWelcomeUrl() {
        return welcomeUrl;
    }

    public void setWelcomeUrl(final String welcomeUrl) {
        this.welcomeUrl = welcomeUrl;
    }

    public String getContactUrl() {
        return contactUrl;
    }

    public void setContactUrl(final String contactUrl) {
        this.contactUrl = contactUrl;
    }
    
    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(final String logoUrl) {
        this.logoUrl = logoUrl;
    }
    
    @Column(columnDefinition = "json")
    @JsonbTypeDeserializer(JsonObjectDeserializer.class)
    public String getInfo() {
        return info;
    }

    @JsonbTypeSerializer(JsonObjectSerializer.class)
    public void setInfo(final String info) {
        this.info = info;
    }
}
