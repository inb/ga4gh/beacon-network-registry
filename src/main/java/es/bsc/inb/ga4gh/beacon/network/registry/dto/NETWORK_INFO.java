/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.registry.dto;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Dmitry Repchevsky
 */

@Entity
@Table(name = "network_info")
public class NETWORK_INFO implements Serializable {

    private String id;
    private String name;
    private String description;
    private SERVICE_ORGANIZATION organization;
    private List<SERVICE_INFO> services;

    @Id
    @Column(nullable = false)
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @ManyToOne(cascade=CascadeType.PERSIST)
    public SERVICE_ORGANIZATION getOrganization() {
        return organization;
    }

    public void setOrganization(final SERVICE_ORGANIZATION organization) {
        this.organization = organization;
    }

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "network_services",
               joinColumns = {@JoinColumn(name = "network_id")},
               inverseJoinColumns = {@JoinColumn(name = "service_id")})
    public List<SERVICE_INFO> getServices() {
        return services;
    }
    
    public void setServices(final List<SERVICE_INFO> services) {
        this.services = services;
    }
}
