/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.registry.dao;

import es.bsc.inb.ga4gh.beacon.network.registry.dto.NETWORK_INFO;
import es.bsc.inb.ga4gh.beacon.network.registry.dto.SERVICE_INFO;
import java.util.Arrays;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class RegistryDAO {
    
    @PersistenceUnit(unitName="REGISTRY")
    private EntityManagerFactory emf;

    public List<SERVICE_INFO> get() {
        final EntityManager em = emf.createEntityManager();

        try {
            final TypedQuery<SERVICE_INFO> query = em.createQuery("SELECT s FROM SERVICE_INFO s", SERVICE_INFO.class);
            return query.getResultList();
        } catch(NoResultException ex) {
            return null;
        } finally {
            em.close();
        }
    }
    
    public List<SERVICE_INFO> get(
            final String serviceType,
            final String apiVersion) {
        final EntityManager em = emf.createEntityManager();

        try {
            final TypedQuery<SERVICE_INFO> query = 
                    em.createQuery("SELECT s FROM SERVICE_INFO s WHERE " +
                            "(:type IS NULL OR s.serviceType = :type) AND" +
                            "(:version IS NULL OR s.apiVersion = :version)", SERVICE_INFO.class)
                    .setParameter("type", serviceType)
                    .setParameter("version", apiVersion);
            
            return query.getResultList();
        } catch(NoResultException ex) {
            return null;
        } finally {
            em.close();
        }
    }


    public SERVICE_INFO get(final String id) {
        final EntityManager em = emf.createEntityManager();

        try {
            final TypedQuery<SERVICE_INFO> query = em.createQuery("SELECT s FROM SERVICE_INFO s WHERE s.id = :id", SERVICE_INFO.class);
            return query.setParameter("id", id).getSingleResult();
        } catch(NoResultException ex) {
            return null;
        } finally {
            em.close();
        }
    }
    
    public int put(final SERVICE_INFO service_info) {
        final int return_code;
        
        final EntityManager em = emf.createEntityManager();

        try {
            em.getTransaction().begin();

            final TypedQuery<SERVICE_INFO> query = em.createQuery("SELECT s FROM SERVICE_INFO s WHERE s.id = :id", SERVICE_INFO.class);
            if (query.setParameter("id", service_info.getId()).getResultList().isEmpty()) {
                em.persist(service_info);
                return_code = 201; // created
            } else {
                em.merge(service_info);
                return_code = 204; // no content
            }
            em.getTransaction().commit();

        } catch (Exception ex) {
            em.getTransaction().rollback();
            return 500;
        } finally {
            em.close();
        }
        return return_code;
    }

    public int putServices(final List<SERVICE_INFO> services) {
        final EntityManager em = emf.createEntityManager();

        try {
            em.getTransaction().begin();

            for (SERVICE_INFO service_info : services) {
                final TypedQuery<SERVICE_INFO> query = em.createQuery("SELECT s FROM SERVICE_INFO s WHERE s.id = :id", SERVICE_INFO.class);
                if (query.setParameter("id", service_info.getId()).getResultList().isEmpty()) {
                    em.persist(service_info);
                } else {
                    em.merge(service_info);
                }
            }
            em.getTransaction().commit();

        } catch (Exception ex) {
            em.getTransaction().rollback();
            return 500;
        } finally {
            em.close();
        }
        return 200;
    }
    
    public int delete(final String id) {
        final EntityManager em = emf.createEntityManager();

        try {
            em.getTransaction().begin();
            final Query query = em.createQuery("DELETE FROM SERVICE_INFO s WHERE s.id = :id");
            final int count = query.setParameter("id", id).executeUpdate();
            em.getTransaction().commit();
            return count == 0 ? 404 : 200;
        } catch (Exception ex) {
            em.getTransaction().rollback();
            return 500;
        } finally {
            em.close();
        }
    }
    
    public List<NETWORK_INFO> getNetworks() {
        final EntityManager em = emf.createEntityManager();

        try {
            final TypedQuery<NETWORK_INFO> query = em.createQuery("SELECT s FROM NETWORK_INFO s", NETWORK_INFO.class);
            return query.getResultList();
        } catch(NoResultException ex) {
            return null;
        } finally {
            em.close();
        }
    }
    
    public int putNetworks(final List<NETWORK_INFO> networks) {
        final EntityManager em = emf.createEntityManager();

        try {
            em.getTransaction().begin();

            for (NETWORK_INFO network_info : networks) {
                final TypedQuery<NETWORK_INFO> query = em.createQuery("SELECT n FROM NETWORK_INFO n WHERE n.id = :id", NETWORK_INFO.class);
                if (query.setParameter("id", network_info.getId()).getResultList().isEmpty()) {
                    em.persist(network_info);

                } else {
                    em.merge(network_info);
                }
            }
            em.getTransaction().commit();

        } catch (Exception ex) {
            em.getTransaction().rollback();
            return 500;
        } finally {
            em.close();
        }
        
        return 200;
    }
    
    public int put(final SERVICE_INFO service_info, final String network_id) {
        final String service_id = service_info.getId();
        if (service_id == null || service_id.isEmpty()) {
            return 400; // bad request
        }
        
        final int return_code;
        
        final EntityManager em = emf.createEntityManager();

        try {
            em.getTransaction().begin();

            final TypedQuery<NETWORK_INFO> query1 = em.createQuery("SELECT s FROM NETWORK_INFO s WHERE s.id = :id", NETWORK_INFO.class);
            final NETWORK_INFO network_info = query1.setParameter("id", network_id).getSingleResult();

            final TypedQuery<SERVICE_INFO> query2 = em.createQuery("SELECT s FROM SERVICE_INFO s WHERE s.id = :id", SERVICE_INFO.class);
            if (query2.setParameter("id", service_info.getId()).getResultList().isEmpty()) {
                em.persist(service_info);
                return_code = 201; // created
            } else {
                em.merge(service_info);
                return_code = 204; // no content
            }
            final List<SERVICE_INFO> services = network_info.getServices();
            if (services == null) {
                network_info.setServices(Arrays.asList(service_info));
            } else {
                int i = services.size() - 1;
                for (; i >= 0; i--) {
                    if (service_id.equals(services.get(i).getId())) {
                        services.set(i, service_info);
                        break;
                    }
                }
                if (i < 0) { // not found
                    services.add(service_info);
                }
            }
            em.merge(network_info);
            
            em.getTransaction().commit();

        } catch(NoResultException ex) {
            return 404;
        } catch (Exception ex) {
            em.getTransaction().rollback();
            return 500;
        } finally {
            em.close();
        }
        return return_code;
    }
}
