/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.registry.rest;

import es.bsc.inb.ga4gh.beacon.network.model.v110.ServiceInfo;
import es.bsc.inb.ga4gh.beacon.network.model.v110.ServiceOrganization;
import es.bsc.inb.ga4gh.beacon.network.model.v110.ServiceTypes;
import es.bsc.inb.ga4gh.beacon.network.registry.dao.RegistryDAO;
import es.bsc.inb.ga4gh.beacon.network.registry.dto.NETWORK_INFO;
import es.bsc.inb.ga4gh.beacon.network.registry.dto.SERVICE_INFO;
import es.bsc.inb.ga4gh.beacon.network.registry.dto.SERVICE_ORGANIZATION;
import es.bsc.inb.ga4gh.service_info.model.v100.Organization;
import es.bsc.inb.ga4gh.service_info.model.v100.Service;
import es.bsc.inb.ga4gh.service_info.model.v100.ServiceType;
import java.net.URI;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

/**
 * @author Dmitry Repchevsky
 */

@Path("/")
@ApplicationScoped
public class BeaconRegistryServices {
    
    private final static String GA4GH_SERVICE_MODEL = "GA4GH-ServiceInfo-v0.1";

    @Inject
    private ServiceInfo service_info;
    
    @Inject
    private RegistryDAO dao;
    
    @Resource
    private ManagedExecutorService executor;
    
    private Service service;

    @PostConstruct
    public void init() {        
        service = new Service();
            
        final String apiVersion = service_info.getApiVersion();
        final String serviceType = service_info.getServiceType();
        
        final ServiceType service_type = new ServiceType();
        service_type.setGroup("org.ga4gh");
        service_type.setVersion(apiVersion);
        
        if (ServiceTypes.GA4GHRegistry.name().equals(serviceType)) {
            service_type.setArtifact("service-registry");
        } else if (ServiceTypes.GA4GHBeaconAggregator.name().equals(serviceType)) {
            service_type.setArtifact("beacon-aggregator");
        } else if (ServiceTypes.GA4GHBeacon.name().equals(serviceType)) {
            service_type.setArtifact("beacon");
        }

        service.setType(service_type);
        
        service.setId(service_info.getId());
        service.setName(service_info.getName());
        service.setVersion(service_info.getVersion());
        service.setDescription(service_info.getDescription());

        final ServiceOrganization serviceOrganization = service_info.getOrganization();
        if (serviceOrganization != null) {
            final Organization organization = new Organization();

            organization.setId(serviceOrganization.getId());

            final String welcomeUrl = serviceOrganization.getWelcomeUrl();
            if (welcomeUrl != null) {
                try {
                    organization.setUrl(URI.create(welcomeUrl));
                } catch(IllegalArgumentException ex) {
                    Logger.getLogger(BeaconRegistryServices.class.getName()).log(
                            Level.INFO, "invalid welcome url", ex.getMessage());
                }
            }

            service.setOrganization(organization);
        }

        final String createDateTime = service_info.getCreateDateTime();
        if (createDateTime != null) {
            try {
                service.setCreatedAt(ZonedDateTime.parse(createDateTime));
            } catch(DateTimeParseException ex) {
                    Logger.getLogger(BeaconRegistryServices.class.getName()).log(
                            Level.INFO, "error parsing datetime", ex.getMessage());
            }
        }

        final String updateDateTime = service_info.getUpdateDateTime();
        if (updateDateTime != null) {
            try {
                service.setUpdatedAt(ZonedDateTime.parse(updateDateTime));
            } catch(DateTimeParseException ex) {
                    Logger.getLogger(BeaconRegistryServices.class.getName()).log(
                            Level.INFO, "error parsing datetime", ex.getMessage());
            }
        }
    }
    
    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceInfo get(@Context UriInfo uri_info) {
        return getInfo(uri_info);
    }
    
    @GET
    @Path("/info")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceInfo getInfo(@Context UriInfo uri_info) {
        final String service_uri = UriBuilder.fromUri(uri_info.getBaseUri())
                .path(BeaconRegistryServices.class).build().toString();
        service_info.setServiceUrl(service_uri);
        return service_info;
    }

    @GET
    @Path("/service-info")
    @Produces(MediaType.APPLICATION_JSON)
    public Service getServiceInfo() {
        return service;
    }

    @GET
    @Path("/service_types")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceTypes[] getServiceTypes() {
        return ServiceTypes.values();
    }

    @GET
    @Path("/services")
    @Produces(MediaType.APPLICATION_JSON)
    public void getServices(
            @Context SecurityContext sc,
            @QueryParam("serviceType") final String serviceType,
            @QueryParam("apiVersion") final String apiVersion,
            @QueryParam("model") final String model,
            @Suspended final AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(getServicesAsync(sc, serviceType, apiVersion, model).build());
        });
    }
    
    @GET
    @Path("/services/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void getService(
            @PathParam("id") final String id,
            @Suspended final AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(getServiceAsync(id).build());
        });
    }
    
    @POST
    @Path("/services")
    @Consumes(MediaType.APPLICATION_JSON)
    public void postServices(
            final List<SERVICE_INFO> service_info,
            @Suspended final AsyncResponse asyncResponse) {

        executor.submit(() -> {
            asyncResponse.resume(postServicesAsync(service_info).build());
        });
    }

    @PUT
    @Path("/services/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void putService(
            @PathParam("id") final String id,
            final SERVICE_INFO service_info,
            @Suspended final AsyncResponse asyncResponse) {
        service_info.setId(id);
        executor.submit(() -> {
            asyncResponse.resume(putServiceAsync(service_info).build());
        });
    }
    
    @DELETE
    @Path("/services/{id}")
    public void deleteService(
            @PathParam("id") final String id,
            @Suspended final AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(deleteServiceAsync(id).build());
        });
    }
    
    @GET
    @Path("/registries")
    @Produces(MediaType.APPLICATION_JSON)
    public void getRegistries(
            @Context SecurityContext sc,
            @Suspended final AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(getServicesAsync(sc, ServiceTypes.GA4GHRegistry.name(), null, null).build());
        });
    }

    @GET
    @Path("/registries/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void getRegistry(
            @PathParam("id") final String id,
            @Suspended final AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(getServiceAsync(id).build());
        });
    }
    
    @GET
    @Path("/networks")
    @Produces(MediaType.APPLICATION_JSON)
    public void getNetworks(
            @Context SecurityContext sc,
            @Suspended final AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(getNetworksAsync(sc).build());
        });
    }

    @POST
    @Path("/networks")
    @Consumes(MediaType.APPLICATION_JSON)
    public void postNetworks(
            final List<NETWORK_INFO> service_info,
            @Suspended final AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(postNetworksAsync(service_info));
        });
    }
    
    @POST
    @Path("/networks/{id}/services")
    @Consumes(MediaType.APPLICATION_JSON)
    public void postService(
            @PathParam("id") final String id,
            final SERVICE_INFO service_info,
            @Suspended final AsyncResponse asyncResponse) {
        executor.submit(() -> {
            asyncResponse.resume(postServiceAsync(service_info, id));
        });
    }

    private Response.ResponseBuilder getServicesAsync(
            final SecurityContext sc,
            final String serviceType,
            final String apiVersion,
            final String model) {
        
        List<SERVICE_INFO> services = serviceType == null && apiVersion == null ? dao.get() : dao.get(serviceType, apiVersion);
       
        // if no login - filter "closed" services
        if (sc.getUserPrincipal() == null) {
            services = new ArrayList<>(services);
            final Iterator<SERVICE_INFO> iter = services.iterator();
            while (iter.hasNext()) {
                if (!Boolean.TRUE.equals(iter.next().getOpen())) {
                    iter.remove();
                }
            }
        }

        if (GA4GH_SERVICE_MODEL.equals(model)) {
            return Response.ok(convert(services));
        }
        
        return Response.ok(services);
    }

    /**
     * Converts ServiceInfo model to the GA4GH Service model
     * 
     * @param services list of ServiceInfos
     * 
     * @return list of GA4GH Services
     */
    private List<Service> convert(final List<SERVICE_INFO> services) {
        final List<Service> list = new ArrayList<>();
        
        for (int i = 0; i < services.size(); i++) {
            final SERVICE_INFO serviceInfo = services.get(i);
            final Service service = new Service();
            
            final String apiVersion = serviceInfo.getApiVersion();
            final String serviceType = serviceInfo.getServiceType();
            
            final ServiceType service_type = new ServiceType();
            service_type.setGroup("org.ga4gh");
            service_type.setVersion(apiVersion);

            if (ServiceTypes.GA4GHRegistry.name().equals(serviceType)) {
                service_type.setArtifact("service-registry");
            } else if (ServiceTypes.GA4GHBeaconAggregator.name().equals(serviceType)) {
                service_type.setArtifact("beacon-aggregator");
            } else if (ServiceTypes.GA4GHBeacon.name().equals(serviceType)) {
                service_type.setArtifact("beacon");
            }
            
            service.setType(service_type);
            
            service.setId(serviceInfo.getId());
            service.setName(serviceInfo.getName());
            service.setVersion(serviceInfo.getVersion());
            service.setDescription(serviceInfo.getDescription());
            
            final SERVICE_ORGANIZATION serviceOrganization = serviceInfo.getOrganization();
            if (serviceOrganization != null) {
                final Organization organization = new Organization();
                
                organization.setId(serviceOrganization.getId());
                
                final String welcomeUrl = serviceOrganization.getWelcomeUrl();
                if (welcomeUrl != null) {
                    try {
                        organization.setUrl(URI.create(welcomeUrl));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BeaconRegistryServices.class.getName()).log(
                                Level.INFO, "invalid welcome url", ex.getMessage());
                    }
                }
                
                service.setOrganization(organization);
            }
            
            final String createDateTime = serviceInfo.getCreateDateTime();
            if (createDateTime != null) {
                try {
                    service.setCreatedAt(ZonedDateTime.parse(createDateTime));
                } catch(DateTimeParseException ex) {
                        Logger.getLogger(BeaconRegistryServices.class.getName()).log(
                                Level.INFO, "error parsing datetime", ex.getMessage());
                }
            }
            
            final String updateDateTime = serviceInfo.getUpdateDateTime();
            if (updateDateTime != null) {
                try {
                    service.setUpdatedAt(ZonedDateTime.parse(updateDateTime));
                } catch(DateTimeParseException ex) {
                        Logger.getLogger(BeaconRegistryServices.class.getName()).log(
                                Level.INFO, "error parsing datetime", ex.getMessage());
                }
            }

            list.add(service);
        }
        
        return list;
    }
    
    private Response.ResponseBuilder getServiceAsync(final String id) {
        final SERVICE_INFO service = dao.get(id);
        if (service == null) {
            return Response.status(Response.Status.NOT_FOUND);
        }
        return Response.ok(service);
    }

    private Response.ResponseBuilder postServicesAsync(final List<SERVICE_INFO> services) {
        return Response.status(dao.putServices(services));
    }
    
    private Response.ResponseBuilder putServiceAsync(final SERVICE_INFO service_info) {
        return Response.status(dao.put(service_info));
    }
    
    private Response.ResponseBuilder deleteServiceAsync(final String id) {
        
        final int status = dao.delete(id);
        if (status == 200 || status == 204) {
            final List<SERVICE_INFO> aggregators = dao.get(ServiceTypes.GA4GHBeaconAggregator.name(), null);
            for (SERVICE_INFO aggregator : aggregators) {
                final String endpoint = aggregator.getServiceUrl();
                if (endpoint != null) {
                    final Client client = ClientBuilder.newClient();
                    try {
                        client.target(UriBuilder.fromUri(endpoint).path("services").path(id)).request().delete();
                    } catch(Exception ex) {
                        Logger.getLogger(BeaconRegistryServices.class.getName()).log(
                                Level.SEVERE, "error removing from {}\n{}", 
                                new String[] {endpoint, ex.getMessage()});
                    }
                }
            }
        }
        
        return Response.status(status);
    }
    
    private Response.ResponseBuilder getNetworksAsync(final SecurityContext sc) {
        final List<NETWORK_INFO> services = dao.getNetworks();        
        return Response.ok(services == null ? Collections.EMPTY_LIST : services);
    }

    private Response.ResponseBuilder postNetworksAsync(final List<NETWORK_INFO> networks) {
        return Response.status(dao.putNetworks(networks));
    }
    
    private Response.ResponseBuilder postServiceAsync(final SERVICE_INFO service_info, final String network_id) {
        return Response.status(dao.put(service_info, network_id));
    }
}
