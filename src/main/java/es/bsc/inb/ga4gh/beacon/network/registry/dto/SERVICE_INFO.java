/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.registry.dto;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Dmitry Repchevsky
 */

@Entity
@Table(name = "service_info")
public class SERVICE_INFO implements Serializable {
    
    private String id;
    private String name;
    private String serviceType;
    private String apiVersion;
    private String serviceUrl;
    private Boolean entryPoint;
    private SERVICE_ORGANIZATION organization;
    private String description;
    private String version;
    private Boolean open;
    private String welcomeUrl;
    private String alternativeUrl;
    private String createDateTime;
    private String updateDateTime;

    public SERVICE_INFO() {}
    
    public SERVICE_INFO(
            final String id,
            final String name,
            final String serviceType,
            final String apiVersion,
            final String serviceUrl,
            final Boolean entryPoint,
            final SERVICE_ORGANIZATION organization,
            final String description,
            final String version,
            final Boolean open,
            final String welcomeUrl,
            final String alternativeUrl,
            final String createDateTime,
            final String updateDateTime) {
        
        this.id = id;
        this.name = name;
        this.serviceType = serviceType;
        this.apiVersion = apiVersion;
        this.serviceUrl = serviceUrl;
        this.entryPoint = entryPoint;
        this.organization = organization;
        this.description = description;
        this.version = version;
        this.open = open;
        this.welcomeUrl = welcomeUrl;
        this.alternativeUrl = alternativeUrl;
        this.createDateTime = createDateTime;
        this.updateDateTime = updateDateTime;
    }
    
    @Id
    @Column(nullable = false)
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(final String serviceType) {
        this.serviceType = serviceType;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(final String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(final String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }
    
    public Boolean getEntryPoint() {
        return entryPoint;
    }

    public void setEntryPoint(final Boolean entryPoint) {
        this.entryPoint = entryPoint;
    }

    @ManyToOne(cascade=CascadeType.PERSIST)
    public SERVICE_ORGANIZATION getOrganization() {
        return organization;
    }

    public void setOrganization(final SERVICE_ORGANIZATION organization) {
        this.organization = organization;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }
    
    public Boolean getOpen() {
        return open;
    }

    public void setOpen(final Boolean open) {
        this.open = open;
    }

    public String getWelcomeUrl() {
        return welcomeUrl;
    }

    public void setWelcomeUrl(final String welcomeUrl) {
        this.welcomeUrl = welcomeUrl;
    }

    public String getAlternativeUrl() {
        return alternativeUrl;
    }

    public void setAlternativeUrl(final String alternativeUrl) {
        this.alternativeUrl = alternativeUrl;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(final String createDateTime) {
        this.createDateTime = createDateTime;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(final String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    @Override
    public int hashCode() {
        return (getId() != null ? getId().hashCode() : 0);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof SERVICE_INFO) {
            final SERVICE_INFO other = (SERVICE_INFO)object;
            return (this.getId() == null && other.getId() != null) || 
                   (this.getId() != null && !this.getId().equals(other.getId()));
        }
        
        return false;
    }

    @Override
    public String toString() {
        return "ServiceInfo[id=" + getId() + "]";
    }

}
