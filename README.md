Beacon Network Registry JAX-RS implementation

The implementation uses Java Persistence API with an external SQL datasource 
connection to the backend database.
**persistence.xml**:
```xml
<jta-data-source>java:/jdbc/beacon-network-registry-ds</jta-data-source>
```
This datasource must be configured by the JEE server.
For instance for the WildFly ***standalone.xml***:
```xml
<subsystem xmlns="urn:jboss:domain:datasources:5.0">
    <datasources>
        <datasource jndi-name="java:/jdbc/beacon-network-registry-ds" pool-name="beacon-network-registry-ds" enabled="true" use-java-context="true" statistics-enabled="${wildfly.datasources.statistics-enabled:${wildfly.statistics-enabled:false}}">
            <connection-url>jdbc:h2:~/beacon.h2.db;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>
            <driver>h2</driver>
            <security>
                <user-name>username</user-name>
                <password>passowrd</password>
            </security>
        </datasource>
        <drivers>
            <driver name="h2" module="com.h2database.h2">
                <xa-datasource-class>org.h2.jdbcx.JdbcDataSource</xa-datasource-class>
            </driver>
        </drivers>
    </datasources>
</subsystem>
```
configures the H2 database conection to the database file located at ~/beacon.h2.db file.

Alternatively, datasource can be configured via WildFly commandline interface (cli):
```shell
>jboss-cli.sh --file=wildfly-datasource.cli
```
where wildfly-datasource.cli:
```shell
connect 127.0.0.1
batch
data-source add --jndi-name=java:/jdbc/beacon-network-registry-ds --name=beacon-network-registry-ds --connection-url=jdbc:h2:~/beacon.h2.db;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE -
run-batch
```

The Registry Application is mapped to the /beacon-network/aggregator/v1.1.0/ path.
